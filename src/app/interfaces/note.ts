export interface Note {
  id: string,
  relid: string,
  message: string
}

import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
import {ApiService} from "../services/api.service";
import {NoteService} from "../services/note.service";
import {Note} from "../interfaces/note";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  noteList: Note[] = [];
  noteRelid: string = this.getUser();
  showAddForm: boolean = false;
  showEditForm: boolean = false;
  errorMessage: string = '';
  editFormValue: string = '';

  constructor(private userService: UserService, private api: ApiService, private noteService: NoteService) {}

  ngOnInit(): void {
    this.api.getManyByFilterTypeRequest('note', this.getUser())
      .subscribe(data => {
        this.noteList = data;
      });
    this.noteService.clearNoteSelection();
  }

  getUser() {
    const userId = this.userService.getUser();
    if(userId === null) {
      return '';
    } else {
      return userId;
    }
  }

  getNote() {
    const noteId = this.noteService.getNote();
    if(noteId === null) {
      return '';
    } else {
      return noteId;
    }
  }

  getNoteIndex(id: string) {
    return this.noteList.findIndex(x => x.id == id)
  }

  getNoteMessage(id: string) {
    const index = this.getNoteIndex(id);
    return this.noteList[index].message;
  }

  setNoteMessage() {
    this.editFormValue = this.getNoteMessage(this.getNote());
  }


  setNote(id: string) {
    this.noteService.setNote(id);
    this.showEditForm = true;
  }

  addNote(form: NgForm) {
    form.value.relid = this.getUser();
    this.api.postTypeRequest('note', form.value)
      .subscribe((res: any) => {
        this.noteList.push({
          id: res.id,
          message: res.message,
          relid: res.relid
        });
        this.showAddForm = false;
        this.errorMessage = '';
      }, err => {
        this.errorMessage = "The message must be no longer than 255 characters."
      })
  }

  deleteNote(id: string) {
      this.api.deleteTypeRequest('note', id)
        .subscribe(() => {
          this.noteList.splice(this.getNoteIndex(id), 1)
        });
  }

  editNote(form: NgForm) {
    form.value.relid = this.getUser();
    const id = this.getNote();
    const index = this.getNoteIndex(this.getNote());

    this.api.patchTypeRequest('note', id, form.value)
      .subscribe((res: any) => {
        this.noteList[index].message = form.value.message;
        this.showEditForm = false;
        this.errorMessage = '';
        this.noteService.clearNoteSelection()
      }, err => {
        this.errorMessage = 'The message must be no longer than 255 characters.';
      })
  }

}

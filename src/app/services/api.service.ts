import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpResponse} from "@angular/common/http";
import { map } from 'rxjs/operators';

import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  getAllTypeRequest(url: string): Observable<[]> {
    return this.http.get<[]>(this.baseUrl + url).pipe(map(res => {
      return res;
    }));
  }

  getManyByFilterTypeRequest(url: string, filter: string): Observable<[]> {
    return this.http.get<[]>(this.baseUrl + url + '?filter=relid||$eq||' + filter).pipe(map(res => {
      return res;
    }))
  }

  postTypeRequest(url: string, payload: any) {
    return this.http.post(this.baseUrl + url, payload).pipe(map(res => {
      return res;
    }));
  }

  deleteTypeRequest(url: string, id: string) {
    return this.http.delete(this.baseUrl + url + '/' + id).pipe(map(res => {
      return res;
    }));
  }

  patchTypeRequest(url: string, id: string, payload: any) {
    return this.http.patch(this.baseUrl + url + '/' + id, payload).pipe(map(res => {
      return res;
    }));
  }

}

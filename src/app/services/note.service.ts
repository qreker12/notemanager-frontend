import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor() { }

  setNote(id: string): void {
    sessionStorage.setItem('activeNote', id);
  }

  getNote(): string | null {
    return sessionStorage.getItem('activeNote');
  }

  clearNoteSelection(): void {
    sessionStorage.removeItem('activeNote');
  }

}

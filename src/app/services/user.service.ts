import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  setUser(username: string): void {
    sessionStorage.setItem('activeUser', username)
  }

  getUser(): string | null {
    return sessionStorage.getItem('activeUser');
  }

  clearUserSelection(): void {
    sessionStorage.removeItem('activeUser');
  }

}

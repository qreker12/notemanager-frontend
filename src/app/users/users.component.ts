import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";

import {User} from "../interfaces/user";
import {ApiService} from "../services/api.service";
import {UserService} from "../services/user.service";
import {findIndex} from "rxjs/operators";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public userList: User[] = [];
  public errorMessage: string = '';
  public showAddForm: boolean = false;

  constructor(private apiService: ApiService, private userService: UserService) { }

  ngOnInit(): void {
    this.apiService.getAllTypeRequest('user').subscribe(res => {
      this.userList = res;
    });
  }

  getUser() {
    const userId = this.userService.getUser();
    if(userId === null) {
      return '';
    } else {
      return userId;
    }
  }


  setUser(id: string) {
    this.userService.setUser(id);
  }

  addUser(form: NgForm) {
    this.apiService.postTypeRequest('user', form.value)
      .subscribe((res: any) => {
          this.userService.setUser(res.username);
          this.userList.push({
            id: res.id,
            username: res.username
          });
          this.showAddForm = false;
          this.errorMessage = '';
      }, err => {
        this.errorMessage = 'Username already exists'
      });
  }

  deleteUser(id: string) {
    this.apiService.deleteTypeRequest('user', id)
      .subscribe(() => {
        this.userList.splice(this.userList.findIndex(x => x.id == id), 1);
        this.userService.clearUserSelection();
      });
  }

}
